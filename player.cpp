#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */

    //testing purposes: check the time used
    clock_t t;
    t = clock();

    this->side = side;
    if (side == WHITE)
    {
        this->enemy_side = BLACK;
    }
    else
    {
        this->enemy_side = WHITE;
    }
    this->board = new Board();

	// this variable possibly used for openings
	this->movecount = 0;
	


    //printout time elapsed
    t = clock() - t;
    //cerr << "constructor: " << ((float)t)/CLOCKS_PER_SEC << " seconds";


}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete board;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 

    // TODO: write a timer to ensure doesn't take too long
    //clock_t t = clock(); 
     
     
    // process opponent's move
    if(opponentsMove != NULL){
	    this->board->doMove(opponentsMove, enemy_side);
	}
	
	// if no available moves, pass
	if(!this->board->hasMoves(this->side)){
		return NULL;
	}

	// loop through moves, decide what's good
    Move *best = new Move(-1, -1);
    best = alphaMiniMax(board, this->side, 6, -10000, 10000, best);
    board->doMove(best, this->side);

    //check time
    //t = clock() - t;
    //int totaltime = (int)((float)t)/((CLOCKS_PER_SEC)/1000);
    //cerr << "Time used: " << totaltime << '\n'
    //    << "Time remaining: " << msLeft;

    return best;
}

Move* Player::alphaMiniMax(Board *board, Side side, int depth, int alpha,
        int beta, Move * best)
{
    if (depth <= 0 || !board->hasMoves(side))
    {
        int bestScore = board->heuristic(this->side);
        best->heur = bestScore;
        return best;
    }
    else if (side == this->side)
    {
        vector<Move *> available = board->getAvailableMoves(side);
        int size = (int)available.size();
        if (size > 10 && depth >= 4)
        {
            depth = depth -2;
        }

        best = available[0];
        best->heur = alpha;
        for (vector<Move*>::iterator it = available.begin();
                it != available.end(); ++it)
        {
            Board *possible = board->copy();
            //make the move
            possible->doMove(*it, this->side);
            Move* temp = alphaMiniMax(possible, this->enemy_side, depth-1,
                    alpha, beta, best);
            int score = temp->heur;
            delete possible;

            if (score > best->heur)
            {
                alpha = score;
                best = *it;
                best->heur = alpha;
            }
            if (alpha >= beta)
            {
                break;
            }
        }
        available.clear();
        return best;
    }
    else //our opponent
    {
        vector<Move *> available = board->getAvailableMoves(side);
        best = available[0];
        best->heur = beta;
        for (vector<Move*>::iterator it = available.begin();
                it != available.end(); ++it)
        {
            Board *possible = board->copy();
            possible->doMove(*it, this->enemy_side);
            Move *temp = alphaMiniMax(possible, this->side, depth-1, alpha,
                    beta, best);
            int score = temp->heur;
            delete possible;

            if (score < beta)
            {
                beta = score;
                best = *it;
                best->heur = beta;
            }
            if (alpha >= beta)
            {
                break;
            }
        }
        available.clear();
        return best;
    }
}           


