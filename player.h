#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <time.h>
#include "common.h"
#include "board.h"

using namespace std;

class Player {

private: 
    vector<vector<Move*> > openingBook;
    vector<Move*> diagonal;
    vector<Move*> parallel;
    vector<Move*> perpendicular;

public:
    Board * board;
    Side side;
    Side enemy_side;
    
    int movecount;

    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    Move *miniMax(Board * board);
    Move *miniMax2(Board *board, Side side, int depth);
    Move *alphaMiniMax(Board *board, Side side, int depth, int alpha,
            int beta, Move * best);
    Move *openings(Board *board, Move *move);
    void clearMoves(vector<Move*> moves);
};

#endif
