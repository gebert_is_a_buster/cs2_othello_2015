#include "board.h"

using namespace std;
/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */

//positional strategy. Corners are the better, centers are bad
int positionalHeuristic[64] = { 
        99,  -8,  8,  6,  6,  8,  -8, 99,
        -8, -24, -4, -3, -3, -4, -24, -8,
        8,  -4,  7,  4,  4,  7,  -4,   8,
        6,  -3,  4,  0,  0,  4,  -3,   6,
        6,  -3,  4,  0,  0,  4,  -3,   6,
        8,  -4,  7,  4,  4,  7,  -4,   8,
        -8, -24, -4, -3, -3, -4, -24, -8,
        99,  -8,  8,  6,  6,  8,  -8, 99
    };
    
int directionsx[8] = {
	-1, -1, -1,
	0,       0,
	1,   1,  1
};

int directionsy[8] = {
	-1, 0, 1,
	-1,    1,
	-1, 0, 1
};
	


Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);

        
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/* Compute heuristic of given state of the board */
int Board::heuristic(Side side){

    //if we're about to lose, do not make this move!!!

    Side other = (side == BLACK) ? WHITE : BLACK;
    if (count(side) == 0)
    {
        return LOSE_CASE;
    }
    //if we're going to win!
    else if (count(other) == 0)
    {
        return WIN_CASE;
    }
    
    //count
    int counts = countBlack() - countWhite();
    if (side == WHITE)
    {
        counts *= -1;
    }
    
    
    /* else, a linear combination of numerous heuristics (from -100 to 100), in order of importance:
     * corner 
     * corner closeness (a negative) 
     * mobility
     * internal
     * static function 
     */
    
    // corner squares
    int corner = 0;
    if(occupied(0, 0)){
		if(get(side, 0, 0)){
		    corner++;
		}
		else{
			corner--;
		}
	}
	if(occupied(7, 0)){
		if(get(side, 7, 0)){
			corner++;
		}
		else{
			corner--;
		}
	}
	if(occupied(0, 7)){
		if(get(side, 0, 7)){
			corner++;
		}
		else{
			corner--;
		}
	}
	if(occupied(7, 7)){
		if(get(side, 7, 7)){
			corner++;
		}
		else{
			corner--;
		}
	}
	
	corner = 25 * corner;
	
	
	// closeness to corner
	int closecorner = 0;
	if(occupied(0, 1)){
		if(get(side, 0, 1)){
			closecorner--;
		}
		else{
			closecorner++;
		}
	}
	if(occupied(1, 0)){
		if(get(side, 1, 0)){
			closecorner--;
		}
		else{
			closecorner++;
		}
	}
	if(occupied(1, 1)){
		if(get(side, 1, 1)){
			closecorner--;
		}
		else{
			closecorner++;
		}
	}
	if(occupied(0, 6)){
		if(get(side, 0, 6)){
			closecorner--;
		}
		else{
			closecorner++;
		}
	}	
	if(occupied(1, 7)){
		if(get(side, 0, 1)){
			closecorner--;
		}
		else{
			closecorner++;
		}
	}
	if(occupied(1, 6)){
		if(get(side,0, 1)){
			closecorner--;
		}
		else{
			closecorner++;
		}
	}
	if(occupied(6, 0)){
		if(get(side,0, 1)){
			closecorner--;
		}
		else{
			closecorner++;
		}
	}
	if(occupied(7, 1)){
		if(get(side, 0 , 1)){
			closecorner--;
		}
		else{
			closecorner++;
		}
	}
	if(occupied(6, 1)){
		if(get(side, 0, 1)){
			closecorner--;
		}
		else{
			closecorner++;
		}
	}
	if(occupied(6, 7)){
		if(get(side, 0, 1)){
			closecorner--;
		}
		else{
			closecorner++;
		}
	}
	if(occupied(7, 6)){
		if(get(side, 0, 1)){
			closecorner--;
		}
		else{
			closecorner++;
		}
	}
	if(occupied(6, 6)){
		if(get(side, 0, 1)){
			closecorner--;
		}
		else{
			closecorner++;
		}
	}
	closecorner = 	8 * closecorner;

    // mobility - our moves / total moves or - opp moves / total moves
    int mob = 0;
    
    if (hasMoves(WHITE) || hasMoves(BLACK))
    {
        int ourmoves = (int) getAvailableMoves(side).size();
        int oppmoves = (int) getAvailableMoves(other).size();
        if(ourmoves > oppmoves){
			mob = 100 * ourmoves / (ourmoves + oppmoves);
		}
		else{
			mob = 100 * oppmoves / (ourmoves + oppmoves);
		}
    }
    
    // static 
    int stat = 0;
    for(int i = 0; i < 8; i++){
		for(int j = 0; j < 8; j++){
		    if(get(side, j, i)){
				stat += positionalHeuristic[8 * i + j];
			}	
		}
	}
	
	// internal - minimize internal disks (not bordering empty squares)
	int frontierheuristic = 0;
	int ourfront = 0;
	int oppfront = 0;
	int borderx = 0;
	int bordery = 0;
	
	for(int i = 0; i < 8; i++){
		for(int j = 0; j < 8; j++){
		    if(get(side, j, i)){
				// iterate over adjacent squares
				for(int k = 0; k < 8; k++){
					borderx = j + directionsx[k];
					bordery = j + directionsy[k];
					if(borderx >= 0 && borderx <=7 && bordery >= 0 && bordery <=7){
						if(!occupied(borderx, bordery)){
							// it is a frontier
							ourfront++;
							break;
						}
					}
				}
			}
			else if(get(other, j, i)){
				for(int k = 0; k < 8; k++){
					borderx = j + directionsx[k];
					bordery = j + directionsy[k];
					if(borderx >= 0 && borderx <=7 && bordery >= 0 && bordery <=7){
						if(!occupied(borderx, bordery)){
							oppfront++;
							break;
						}
					}
				}
			}
		}
	}
	
    if (ourfront != 0 || oppfront != 0)
    {
	if(ourfront > oppfront){
		frontierheuristic = -100 * ourfront / (oppfront + ourfront); 
	}
	else{
		frontierheuristic = 100 * oppfront / (oppfront + ourfront);
	}
    }
    
    
    return 80 * corner + 30 * closecorner + 10 * frontierheuristic + 
        mob/2 + stat + 3*counts;
    
    
}	



/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/* 
 * Returns a vector of available moves
 */
std::vector<Move*> Board::getAvailableMoves(Side side){
	// simply loop through all squares
	std::vector<Move*> available;
	
	for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move* move = new Move(i, j);
            if(checkMove(move, side)){
				available.push_back(move);				
			}
        }
    }
    return available;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}
